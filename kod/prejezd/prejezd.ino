#include <Servo.h>

int pos = 0;
int red1 = 7;
int red2 = 8;
int white = 6;
int butt = 2;
bool state = false;

Servo servo_9;


void setup()
{
  Serial.begin(9600);
  servo_9.attach(9);
  servo_9.write(pos);
  pinMode(red1, OUTPUT);
  pinMode(red2, OUTPUT);
  pinMode(white, OUTPUT);
  pinMode(butt, INPUT_PULLUP);

  //přiřazení attachInterrupt na button při stoupání
  attachInterrupt(digitalPinToInterrupt(butt), buttonPressed, RISING);
  
  digitalWrite(red1, LOW);
  digitalWrite(red2, LOW);
  digitalWrite(white, LOW);
}

void loop()
{
  // kontrola stavu zmáčknutí
  if(state){
    digitalWrite(red1, HIGH);
    
    // postupné klesání závory
    for (pos = 0; pos <= 90; pos += 1)
  {
    // každých 10 stupňů se vymění hodnoty na led
    if ((pos % 10) == 0)
    {
      digitalWrite(red1, !digitalRead(red1));
      digitalWrite(red2, !digitalRead(red2));
    }
    servo_9.write(pos);
    delay(50);
  }

  // probliknutí zabezpečovacího zařízení při průjezdu vlaku
  for (int i = 0; i < 4; i++)
  {
    digitalWrite(red1, !digitalRead(red1));
    digitalWrite(red2, !digitalRead(red2));
    delay(500);
  }
  
  //postupné stoupání závory
  for (pos = 90; pos >= 0; pos -= 1)
  {
    if ((pos % 10) == 0)
    {
      digitalWrite(red1, !digitalRead(red1));
      digitalWrite(red2, !digitalRead(red2));
    }
    servo_9.write(pos);
    delay(50);
  }
  // vypnutí červených led
    digitalWrite(red1, LOW);
    digitalWrite(red2, LOW);

    // zmáčknuto na false
    state = false;
  }
    // blikání bílé led při normálním provozu
   	digitalWrite(white, !digitalRead(white));
  	delay(500);
  	digitalWrite(white, LOW);
	delay(500); 
  
  
  
}

// metoda která se provede při stisku tlačítka
void buttonPressed()
{

  state = true;
}
